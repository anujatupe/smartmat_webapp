$(function(){

	var change_progress = function(milk_left) {
		var milk_left = parseInt(parseFloat(milk_left)*100);
		var percent_milk = milk_left.toString()+"%";

		$("#full_progress").css({'width': percent_milk});
		$("#progress_words").text(percent_milk+' milk left');
		if(milk_left <= 25) {
			$("#order_now_button").removeAttr("disabled");
		} else {
			$("#order_now_button").attr('disabled', 'disabled');
		}
	};

	var segregate_data = function(milk_data) {
		var data = {}
		for (var i = 0; i<milk_data.length; i++){
			var created_at = milk_data[i]["created_at"];
			created_at = created_at.substring(0,10);

			if (data[created_at] == null) {
				data[created_at] = [];
			} 
			data[created_at].push(parseInt(parseFloat(milk_data[i]["value"])*100));
			
		}

		console.log("Segregated data:");
		console.log(data);

		return data;
	}

	var aggregate_data = function(segregated_data) {
		var all_dates = [];
		var avgs = [];
		for (date in segregated_data) {
			all_dates.push(date);
			var avg_val = 0.0;
			for(var j = 0; j < segregated_data[date].length; j++) {
				avg_val += segregated_data[date][j];
			}
			avg_val = avg_val/(segregated_data[date].length);
			avgs.push(avg_val);
		}
		return {'dates': all_dates, 'avgs': avgs};
	}

	
	var ctx = $("#myChart").get(0).getContext("2d");

	var get_sensor_data = function (chart_options) {
		$.ajax({
            type:"GET",
            url:"http://api.smartify.info/items.json",
            dataType:"json",
            success: function(records){
             	var latest_amount_left = records[records.length-1]['value'];
				change_progress(latest_amount_left);
            	var segregated_data = segregate_data(records);
            	var aggregated_data = aggregate_data(segregated_data);
            	var data = {
				    labels: aggregated_data['dates'],
				    datasets: [
				        {
				            label: "My Second dataset",
				            fillColor: "rgba(151,187,205,0.2)",
				            strokeColor: "rgba(151,187,205,1)",
				            pointColor: "rgba(151,187,205,1)",
				            pointStrokeColor: "#fff",
				            pointHighlightFill: "#fff",
				            pointHighlightStroke: "rgba(151,187,205,1)",
				            data: aggregated_data['avgs']
				        }
				    ]
				};
            	var myLineChart = new Chart(ctx).Line(data, chart_options);

            },
            error: function(records) {
            	alert("Oops!! Could not retrieve data! Try again!")
            }
        });
	}


	var options = {

	    ///Boolean - Whether grid lines are shown across the chart
	    scaleShowGridLines : true,

	    //String - Colour of the grid lines
	    scaleGridLineColor : "rgba(0,0,0,.05)",

	    //Number - Width of the grid lines
	    scaleGridLineWidth : 1,

	    //Boolean - Whether to show horizontal lines (except X axis)
	    scaleShowHorizontalLines: true,

	    //Boolean - Whether to show vertical lines (except Y axis)
	    scaleShowVerticalLines: true,

	    //Boolean - Whether the line is curved between points
	    bezierCurve : false,

	    //Number - Tension of the bezier curve between points
	    bezierCurveTension : 0.4,

	    //Boolean - Whether to show a dot for each point
	    pointDot : true,

	    //Number - Radius of each point dot in pixels
	    pointDotRadius : 4,

	    //Number - Pixel width of point dot stroke
	    pointDotStrokeWidth : 1,

	    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
	    pointHitDetectionRadius : 20,

	    //Boolean - Whether to show a stroke for datasets
	    datasetStroke : true,

	    //Number - Pixel width of dataset stroke
	    datasetStrokeWidth : 2,

	    //Boolean - Whether to fill the dataset with a colour
	    datasetFill : true,

	    //String - A legend template
	    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

	};

	$("#order_now_button").attr('disabled', 'disabled');
	get_sensor_data(options);
	setInterval(function() {get_sensor_data(options);}, 15000);

});