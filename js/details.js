$(function() {

	var segregate_data = function(milk_data) {
		var data = {}
		for (var i = 0; i<milk_data.length; i++){
			var created_at = milk_data[i]["created_at"];

			if (data[created_at] == null) {
				data[created_at] = [];
			} 
			data[created_at] = {'amount_left': parseInt(parseFloat(milk_data[i]["value"])*100),
								   'date': created_at.substring(0,10),
								   'time': created_at.substring(11, created_at.length-8)};
			
		}

		console.log("Segregated data:");
		console.log(data);
		return data;
	}

	var fill_details_section = function(data) {
		var key;
		var i =0;
		for(key in data){
			var details_number = "details_"+i.toString();
			$("#details_container").append("<div id="+details_number+"></div>");
			$("#"+details_number).append("<div class='date'>Date - "+data[key]['date']+"</div>");
			$("#"+details_number).append("<div class='time'>Time - "+data[key]['time']+"</div>");
			$("#"+details_number).append("<div class='percent'>"+data[key]['amount_left']+"% milk left</div>");
			$("#details_container").append("<hr>");
			i++;
		}
		
	}

	var get_milk_details = function () {
		$.ajax({
            type:"GET",
            url:"http://api.smartify.info/items.json",
            dataType:"json",
            success: function(records){
            	var data = segregate_data(records);
            	fill_details_section(data);
            },
            error: function(records) {
            }
        });
	}

	get_milk_details();

});